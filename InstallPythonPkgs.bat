@ECHO OFF
:FOR /F "tokens=1-5" %%A IN ("This is a short sentence") DO @echo %%A %%B %%D
:for /F "tokens=*" %%A in (myfile.txt) do [process] %%A
:for /F "usebackq tokens=*" %%A in ("my file.txt") do [process] %%A REM if spaces in the filenames

REM upgrade pip first (avoid the warning)
python -m pip install --upgrade pip

REM read packages from the filelist and install them
for /F "tokens=*" %%A in (pythonPkgs.txt) do (echo Python package : %%A & pip install %%A & echo. )

REM upgrade all local installed packages
pip-review --local --interactive